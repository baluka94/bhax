
#ifndef LZWBINFA_H
#define LZWBINFA_H

#include <string>

/**
  * class LZWBinFa
  * Lérehozunk még egy destrkutort is amiben meghívjuk a szabadit() függvényt a
  * gyoker egyes és nullas gyermekeire
  */

class LZWBinFa
{
public:

  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  LZWBinFa ();

  /**
   * Empty Destructor
   */
  virtual ~LZWBinFa ();

  // Static Public attributes
  //  

  // Public attributes
  //  


  // Public attribute accessor methods
  //  


  // Public attribute accessor methods
  //  



  /**
   * @param  b
   */
  void operator_ (char b)
  {
  }


  /**
   * @return int
   */
  int getMelyseg ()
  {
  }


  /**
   * @return std::ostream&
   * @param  os
   * @param  bf
   */
  std::ostream& operator_ (std::ostream& os, LZWBinFa& bf)
  {
  }


  /**
   * @param  os
   */
  void kiir (std::ostream& os)
  {
  }

protected:

  // Static Protected attributes
  //  

  // Protected attributes
  //  

  // A fában a gyökér csomópont ki van tüntetve 
  Csomopont gyoker;
  int maxMelyseg;
public:


  // Protected attribute accessor methods
  //  

protected:

public:


  // Protected attribute accessor methods
  //  


  /**
   * Set the value of gyoker
   * A fában a gyökér csomópont ki van tüntetve
   * @param new_var the new value of gyoker
   */
  void setGyoker (Csomopont new_var)   {
      gyoker = new_var;
  }

  /**
   * Get the value of gyoker
   * A fában a gyökér csomópont ki van tüntetve
   * @return the value of gyoker
   */
  Csomopont getGyoker ()   {
    return gyoker;
  }

  /**
   * Set the value of maxMelyseg
   * @param new_var the new value of maxMelyseg
   */
  void setMaxMelyseg (int new_var)   {
      maxMelyseg = new_var;
  }

  /**
   * Get the value of maxMelyseg
   * @return the value of maxMelyseg
   */
  int getMaxMelyseg ()   {
    return maxMelyseg;
  }
protected:



  /**
   * @param  elem
   */
  void rmelyseg (Csomopont* elem)
  {
  }

private:

  // Static Private attributes
  //  

  // Private attributes
  //  

  // Mindig az algoritumus szerint megfelelő Csomopontra mutat
  Csomopont* fa;
  int melyseg;
public:


  // Private attribute accessor methods
  //  

private:

public:


  // Private attribute accessor methods
  //  


  /**
   * Set the value of fa
   * Mindig az algoritumus szerint megfelelő Csomopontra mutat
   * @param new_var the new value of fa
   */
  void setFa (Csomopont* new_var)   {
      fa = new_var;
  }

  /**
   * Get the value of fa
   * Mindig az algoritumus szerint megfelelő Csomopontra mutat
   * @return the value of fa
   */
  Csomopont* getFa ()   {
    return fa;
  }

  /**
   * Set the value of melyseg
   * @param new_var the new value of melyseg
   */
  void setMelyseg (int new_var)   {
      melyseg = new_var;
  }

  /**
   * Get the value of melyseg
   * @return the value of melyseg
   */
  int getMelyseg ()   {
    return melyseg;
  }
private:



  /**
   * @param
   */
   LZWBinFa (const LZWBinFa& )
  {
  }


  /**
   * Másolókonstruktor ismételt letiltása
   * @return LZWBinFa&
   * @param
   */
  LZWBinFa& operator_ (const LZWBinFa& )
  {
  }


  /**
   * @param  elem
   * @param  os
   */
  void kiir (Csomopont* elem, std::ostream& os)
  {
  }


  /**
   * @param  elem
   */
  void szabadit (Csomopont* elem)
  {
  }

  void initAttributes () ;

};

#endif // LZWBINFA_H
