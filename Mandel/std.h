
#ifndef STD_H
#define STD_H

#include <string>

/**
  * class std
  * 
  */

class std
{
public:

  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  std ();

  /**
   * Empty Destructor
   */
  virtual ~std ();

  // Static Public attributes
  //  

  // Public attributes
  //  


  // Public attribute accessor methods
  //  


  // Public attribute accessor methods
  //  


protected:

  // Static Protected attributes
  //  

  // Protected attributes
  //  

public:


  // Protected attribute accessor methods
  //  

protected:

public:


  // Protected attribute accessor methods
  //  

protected:


private:

  // Static Private attributes
  //  

  // Private attributes
  //  

public:


  // Private attribute accessor methods
  //  

private:

public:


  // Private attribute accessor methods
  //  

private:



};

#endif // STD_H
