//g++ jdk_classes.cpp -o jdk_classes -lboost_system -lboost_filesystem -lboost_program_options -std=c++14
//./jdk_classes mappa_nev


#include <iostream>
#include <string>
#include <fstream>
#include <iomanip>
#include <vector>

#include "boost_1_71_0/boost/filesystem.hpp"


using namespace std;



void read_file ( boost::filesystem::path path,int &szamlalo )
{

        if ( is_regular_file ( path ) ) {

                string ext ( ".java" );
                if ( !ext.compare ( boost::filesystem::extension ( path ) ) ) {

                        string actjavaspath = path.string();
                    size_t end = actjavaspath.find_last_of ( "/" );
                    string act = actjavaspath.substr ( 0, end );

                        
                        szamlalo++;

                }

        } else if ( is_directory ( path ) )
                for ( boost::filesystem::directory_entry & entry : boost::filesystem::directory_iterator ( path ) )
                        read_file ( entry.path(), szamlalo );

}

int main( int argc, char *argv[])
{
    
    system("mkdir src");
    system("cd src");
    system("unzip src.zip -d src");	
	string path="src";
	boost::filesystem::path a(argv[1]);
    int szamlalo=0;
    read_file(a, szamlalo);
    cout<< "Az src.zip-ben található java osztályok száma: "<< szamlalo <<endl;
    return 0;

}
